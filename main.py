import numpy as np
import matplotlib.pyplot as plt
from tools.prepare_mnist import MNIST

def main():
    mnist = MNIST()

    # Loading the data
    X_train, y_train, X_test, y_test, classes = mnist.load()

if __name__ == '__main__':
    main()
